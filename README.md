Libwiki
-------
`Libwiki` is a Rust library for efficiently processing MediaWiki databases.  More specifically, this project is designed for machine learning applications where it can be useful to train models on large amounts of text (such as those found on Wikipedia).

The English language wikipedia has over 12 million pages, and a fully installed MySQL database of the English language wikipedia is about 50GB. It's difficult to process content on that scale, and that's where `libwiki` comes in.

I initially wrote `libwiki` when accessing thousands of Wikipedia articles from Python became too cumbersome. `libwiki` is designed to abstract away the messy details into a model that is easily consumable from a higher level language like Python.

Naturally, `libwiki` needs a running MySQL installation of Wikipedia. You can get a database dump [here](https://meta.wikimedia.org/wiki/Data_dumps),  and tools for installing it can be found [here](https://www.mediawiki.org/wiki/Manual:Importing_XML_dumps). N.B., I have had the most success with [xml2sql](https://meta.wikimedia.org/wiki/Data_dumps/xml2sql).
