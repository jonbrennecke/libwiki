RUST = stable
MULTIRUST = multirust run $(RUST)
CARGO = $(MULTIRUST) cargo
FEATURES = --features c_api

# lib
build_lib:
	$(CARGO) build --lib $(FEATURES)

test_lib:
	$(CARGO) test --lib $(FEATURES)

# kafka
build_kafka:
	$(CARGO) build --bin libwiki_kafka

run_kafka:
	$(CARGO) run --bin libwiki_kafka
	
# release:
# 	$(CARGO) build --release	