use util::{BlockIterator, FlatIterator, IntoFlatMap};
use models::PageTextCategory;

pub struct PageCollectionIterator(FlatIterator<Iterator<Item=PageTextCategory>>);

impl PageCollectionIterator {
	fn new(iter: BlockIterator) -> Self {
		PageCollectionIterator(FlatIterator::<_>(iter.into_flat_map()))
	}
}

impl Iterator for PageCollectionIterator {
	type Item = PageTextCategory;

	fn next(&mut self) -> Option<PageTextCategory> {
		self.0.next()
	}
}

pub struct PageCollection(BlockIterator);

impl PageCollection {
	fn new(iter: BlockIterator) -> Self {
		PageCollection(iter)
	}
}

impl IntoIterator for PageCollection {
    type Item = PageTextCategory;
    type IntoIter = PageCollectionIterator;

    fn into_iter(self) -> Self::IntoIter {
    	PageCollectionIterator::new(self.0)
    }
}

/*=============================
=            C API            =
=============================*/

#[cfg(feature = "c_api")]
mod extern_c {
	use super::*;
	use util::BlockIterator;
	use models::PageTextCategory;
	use connection::Connection;

	#[no_mangle]
	pub unsafe extern "C" fn new_page_collection(conn: * mut Connection) 
		-> * mut PageCollection
	{
		let boxed = Box::from_raw(conn);
		let collection = PageCollection::new(BlockIterator::new(*boxed, 1000));
		Box::into_raw(Box::new(collection)) as * mut PageCollection
	}

	#[no_mangle]
	pub unsafe extern "C" fn new_page_collection_iterator(collection: * mut PageCollection) 
		-> * mut PageCollectionIterator
	{
		let b = Box::from_raw(collection);
		Box::into_raw(Box::new(b.into_iter())) as * mut PageCollectionIterator
	}

	#[no_mangle]
	pub unsafe extern "C" fn page_collection_iterator_get_next(iter: * mut PageCollectionIterator) 
		-> * mut PageTextCategory 
	{
		let b = Box::new((*iter).next());
		Box::into_raw(b) as * mut PageTextCategory
	}
}

#[cfg(feature = "c_api")]
pub use self::extern_c::*;

/*=====  End of C API  ======*/

#[cfg(feature = "c_api")]
#[cfg(test)]
mod test {
	use super::*;
	use libc;
	use util::{BlockIterator, maybe_connect};
	use connection::{wiki_config, wiki_connect};
	use std::ptr;
	use std::ffi::CString;

	#[allow(non_camel_case_types)]
	pub type c_string = * const libc::c_char;

	#[test]
	fn test_text_cat_iter() {
		let conn = maybe_connect();
		let block_iter = BlockIterator::new(conn, 100);
		let collection = PageCollection::new(block_iter);
		let mut iter = collection.into_iter();
		let maybe_ptc = iter.next();
		assert!(maybe_ptc.is_some());
		let ptc = maybe_ptc.unwrap();
		assert!(ptc.page.title != "");
	}

	#[test]
	fn test_c_api() {
		unsafe {
			let host = "127.0.0.1";
			let user = "root";
			let pwd = "";
			let port = 3306;
			let dbname = "wiki";
			let config = wiki_config(
				CString::new(host).unwrap().as_ptr(), 
				CString::new(user).unwrap().as_ptr(), 
				CString::new(pwd).unwrap().as_ptr(), 
				port, 
				CString::new(dbname).unwrap().as_ptr());
			let conn = wiki_connect(config);
			assert!(conn != ptr::null_mut());
			let collection = new_page_collection(conn);
			assert!(collection != ptr::null_mut());

			let iter = new_page_collection_iterator(collection);
			assert!(iter != ptr::null_mut());

			let next = page_collection_iterator_get_next(iter);
			assert!(next != ptr::null_mut());

			let ptc = Box::from_raw(next);
			assert!(ptc.page.title != "");
		}
	}
}