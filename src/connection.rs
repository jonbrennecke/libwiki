use std::default::Default;
use mysql::conn::MyOpts;
use mysql::conn::pool::MyPool;

use error::Error;

#[derive(Debug)]
pub struct ConnectionConfig {
	pub host: String,
	pub user: String,
	pub pwd: String,
	pub port: u16,
	pub dbname: String
}

impl ConnectionConfig {
	pub fn new(
		host: String,
		user: String,
		pwd: String,
		port: u16,
		dbname: String
	) -> ConnectionConfig {
		ConnectionConfig {
			host: host,
			user: user,
			pwd: pwd,
			port: port,
			dbname: dbname,
		}
	}
}

impl Default for ConnectionConfig {
	fn default() -> ConnectionConfig {
		ConnectionConfig {
			host: "127.0.0.1".to_string(),
			user: "root".to_string(),
			pwd: "".to_string(),
			port: 3306,
			dbname: "wiki".to_string()
		}
	}
}

pub struct Connection {
	pub pool: MyPool
}

impl Connection {
	pub fn new(config: ConnectionConfig) -> Result<Self, Error> {
		let opts = MyOpts {
			tcp_addr: Some(config.host),
			tcp_port: config.port,
			user: Some(config.user),
			db_name: Some(config.dbname),
			pass: Some(config.pwd),
			..Default::default()
		};
		let pool = try!(MyPool::new(opts));

		Ok(Connection {
			pool: pool
		})
	}
}

/*=============================
=            C API            =
=============================*/

#[cfg(feature = "c_api")]
mod extern_c {
	use super::*;
	use libc;
	use std::ffi::CStr;
	use std::str;
	use std::ptr;

	macro_rules! try_or_null {
	    ($expr:expr, $convert:ty) => (match $expr {
	        Result::Ok(val) => val,
	        Result::Err(_) => {
	            return ptr::null_mut() as * mut $convert;
	        }
	    })
	}

	#[no_mangle]
	pub unsafe extern "C" fn wiki_config(
		host: * const libc::c_char,
		user: * const libc::c_char,
		pwd: * const libc::c_char,
		port: u16,
		dbname: * const libc::c_char
	) -> * mut ConnectionConfig {

		let host_slice = CStr::from_ptr(host);
		let user_slice = CStr::from_ptr(user);
		let pwd_slice = CStr::from_ptr(pwd);
		let dbname_slice = CStr::from_ptr(dbname);

		let host_str = try_or_null!(str::from_utf8(host_slice.to_bytes()), ConnectionConfig);
		let user_str = try_or_null!(str::from_utf8(user_slice.to_bytes()), ConnectionConfig);
		let pwd_str = try_or_null!(str::from_utf8(pwd_slice.to_bytes()), ConnectionConfig);
		let dbname_str = try_or_null!(str::from_utf8(dbname_slice.to_bytes()), ConnectionConfig);

		let b = Box::new(ConnectionConfig::new(
			host_str.to_owned(), 
			user_str.to_owned(), 
			pwd_str.to_owned(), 
			port, 
			dbname_str.to_owned()
		));

		Box::into_raw(b) as * mut ConnectionConfig
	}

	#[no_mangle]
	pub unsafe extern "C" fn wiki_connect(config_ptr: * mut ConnectionConfig) -> * mut Connection {
		let b = Box::from_raw(config_ptr);
		match Connection::new(*b) {
			Ok(conn) => {
				let b = Box::new(conn);
				Box::into_raw(b) as * mut Connection
			}
			Err(_) => ptr::null_mut() as * mut Connection
		}
	}
}

#[cfg(feature = "c_api")]
pub use self::extern_c::*;

/*=====  End of C API  ======*/

#[cfg(feature = "c_api")]
#[cfg(test)]
mod tests {
	use super::*;
	use std::ptr;
	use std::ffi::CString;

	#[test]
	fn it_connects() {
		let conn = Connection::new(Default::default());
		assert!(conn.is_ok());
	}

	#[test]
	fn test_wiki_config() {
		unsafe {
			let host = "127.0.0.1";
			let user = "root";
			let pwd = "";
			let port = 3306;
			let dbname = "wiki";
			let config_ptr = wiki_config(
				CString::new(host).unwrap().as_ptr(), 
				CString::new(user).unwrap().as_ptr(), 
				CString::new(pwd).unwrap().as_ptr(), 
				port, 
				CString::new(dbname).unwrap().as_ptr());
			let ref config = * config_ptr;
			assert_eq!(config.host, host);
			assert_eq!(config.user, user);
			assert_eq!(config.pwd, pwd);
			assert_eq!(config.dbname, dbname);
		};
	}

	#[test]
	fn test_c_api() {
		unsafe {
			let host = "127.0.0.1";
			let user = "root";
			let pwd = "";
			let port = 3306;
			let dbname = "wiki";
			let config = wiki_config(
				CString::new(host).unwrap().as_ptr(), 
				CString::new(user).unwrap().as_ptr(), 
				CString::new(pwd).unwrap().as_ptr(), 
				port, 
				CString::new(dbname).unwrap().as_ptr());
			let connection = wiki_connect(config);
			assert!(connection != ptr::null_mut());
		}
	}
}