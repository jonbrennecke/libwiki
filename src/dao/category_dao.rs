use mysql::value::from_value;

use connection::Connection;
use error::Error;
use models::Category;
use util::maybe_get_column_idx;

pub struct CategoryDao {
	pub conn: Connection
}

impl CategoryDao {
	pub fn new(conn: Connection) -> Self {
		CategoryDao {
			conn: conn
		}
	}

	pub fn get_by_id(&mut self, id: u64) -> Result<Vec<Category>, Error> {
		let ref pool = self.conn.pool;
		let results = try!(pool.prep_exec(
			r"select c.* from category where c.cat_id = ?", (id,))
			.map_err(|e| Error::from(e)));

		let id_idx = try!(maybe_get_column_idx(&results, "cat_id"));
		let title_idx = try!(maybe_get_column_idx(&results, "cat_title"));
		let pages_idx = try!(maybe_get_column_idx(&results, "cat_pages"));

		Ok(results.map(|maybe_row| {
			let row = maybe_row.unwrap();
			Category {
				id: from_value(row[id_idx].to_owned()),
				title: from_value(row[title_idx].to_owned()),
				pages: from_value(row[pages_idx].to_owned()),
			}
		})
		.collect::<Vec<Category>>())
	}
}