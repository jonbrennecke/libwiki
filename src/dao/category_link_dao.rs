use mysql::value::from_value;

use connection::Connection;
use error::Error;
use models::CategoryLink;
use util::maybe_get_column_idx;

pub struct CategoryLinkDao {
	pub conn: Connection
}

impl CategoryLinkDao {
	pub fn new(conn: Connection) -> Self {
		CategoryLinkDao {
			conn: conn
		}
	}

	pub fn get_by_page_id(&mut self, page_id: u64) -> Result<Vec<CategoryLink>, Error> {
		let ref pool = self.conn.pool;
		let results = try!(pool.prep_exec(
			r"select c.* from categorylinks c where c.cl_from = ?", (page_id,))
			.map_err(|e| Error::from(e)));

		let from_idx = try!(maybe_get_column_idx(&results, "cl_from"));
		let to_idx = try!(maybe_get_column_idx(&results, "cl_to"));

		Ok(results.map(|maybe_row| {
			let row = maybe_row.unwrap();
			CategoryLink {
				to: from_value(row[to_idx].to_owned()),
				from: from_value(row[from_idx].to_owned()),
			}
		})
		.collect::<Vec<CategoryLink>>())
	}
}

#[cfg(test)]
mod test {
	use super::*;
	use util::maybe_connect;

	#[test]
	fn test_get_cat_by_page_id() {
		let conn = maybe_connect();
		let mut cat_dao = CategoryLinkDao::new(conn);
		let res = cat_dao.get_by_page_id(1);
		assert!(res.is_ok());
		res.unwrap();
	}
}