mod page_dao;
mod revision_dao;
mod text_dao;
mod category_dao;
mod category_link_dao;
mod page_text_category_dao;

pub use self::page_dao::*;
pub use self::revision_dao::*;
pub use self::text_dao::*;
pub use self::category_dao::*;
pub use self::category_link_dao::*;
pub use self::page_text_category_dao::*;

#[cfg(test)]
mod test {
	// use super::*;
	// use util::maybe_connect;

	#[test]
	fn test_get_text_for_revision() {
		// let conn = maybe_connect();
		// let mut revision_dao = RevisionDao::new(conn.clone());
		// let mut text_dao = TextDao::new(conn);
		// let maybe_block = revision_dao.get_block(1, 2);
		// assert!(maybe_block.is_ok());
		// let block = maybe_block.unwrap();
		// for rev in block {
		// 	let maybe_revisions = revision_dao.get_by_id(rev.id);
		// 	assert!(maybe_revisions.is_ok());
		// 	let revisions = maybe_revisions.unwrap();
		// 	assert!(revisions.len() > 0);
		// 	let ref revision = revisions[0];
		// 	let maybe_texts = text_dao.get_by_id(revision.text_id);
		// 	assert!(maybe_texts.is_ok());
		// 	let texts = maybe_texts.unwrap();
		// 	assert!(texts.len() > 0);
		// }
	}
}