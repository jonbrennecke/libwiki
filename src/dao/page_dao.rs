use connection::Connection;
use error::Error;
use mysql::conn::QueryResult;

pub struct PageDao {
	pub conn: Connection
}

impl PageDao {
	pub fn new(conn: Connection) -> Self {
		PageDao {
			conn: conn
		}
	}

	pub fn get_block(&mut self, index: usize, block_size: usize) -> Result<QueryResult, Error> {
		let ref pool = self.conn.pool;
		return pool.prep_exec(
			r"select p.* from page p limit ?, ?", (index, block_size))
			.map_err(|e| Error::from(e));
	}
}
