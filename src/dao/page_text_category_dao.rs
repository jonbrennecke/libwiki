use models::*;
use error::Error;
use connection::Connection;

pub struct PageTextCategoryDao {
	conn: Connection,
}

impl<'a> PageTextCategoryDao {
	pub fn new(conn: Connection) -> Self {
		PageTextCategoryDao {
			conn: conn
		}
	}

	pub fn get_several(&mut self, index: usize, page_limit: usize) 
		-> Result<Vec<PageTextCategory>, Error> 
	{
		let query_str = "select * from categorylinks cl inner join (select * from page p
			left join revision r on p.page_latest=r.rev_id 
			left join text t on r.rev_text_id=t.old_id group by p.page_id limit ?,?) 
			p on cl.cl_from=p.page_id;";

		let results = try!(self.conn.pool.prep_exec(query_str, (index, page_limit)).map_err(|e| Error::from(e)));
		let page_idxs = try!(PageColumnIndexes::new(&results));
		let txt_idxs = try!(TextColumnIndexes::new(&results));
		let cl_idxs = try!(CategoryLinkColumnIndexes::new(&results));

		Ok(results.map(|result| {
			let row = result.unwrap();
			let page = Page::new_with_row_indexes(&row, &page_idxs);
			let txt = Text::new_with_row_indexes(&row, &txt_idxs);
			let cl = CategoryLink::new_with_row_indexes(&row, &cl_idxs);
			PageTextCategory {
				page: page,
				category_link: cl.ok(),
				text: txt.ok(),
			}
		}).collect())
	}
}