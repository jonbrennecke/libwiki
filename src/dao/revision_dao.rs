use mysql::value::from_value;

use connection::Connection;
use error::Error;
use models::Revision;
use util::maybe_get_column_idx;

pub struct RevisionDao {
	pub conn: Connection
}

impl RevisionDao {
	pub fn new(conn: Connection) -> Self {
		RevisionDao {
			conn: conn
		}
	}

	pub fn get_by_id(&mut self, id: u64) -> Result<Vec<Revision>, Error> {
		let ref pool = self.conn.pool;
		let results = try!(pool.prep_exec(
			r"select r.* from revision r where r.rev_id = ? limit ?", (id, 1))
			.map_err(|e| Error::from(e)));

		let rev_id_idx = try!(maybe_get_column_idx(&results, "rev_id"));
		let rev_text_id_idx = try!(maybe_get_column_idx(&results, "rev_text_id"));

		Ok(results.map(|maybe_row| {
			let row = maybe_row.unwrap();
			Revision {
				id: from_value(row[rev_id_idx].to_owned()),
				text_id: from_value(row[rev_text_id_idx].to_owned()),
			}
		})
		.collect::<Vec<Revision>>())
	}

	pub fn get_block(&mut self, index: usize, block_size: usize) -> Result<Vec<Revision>, Error> {
		let ref pool = self.conn.pool;
		let results =  try!(pool.prep_exec(
			r"select r.* from revision r limit ?, ?", (index, block_size))
			.map_err(|e| Error::from(e)));

		let rev_id_idx = try!(maybe_get_column_idx(&results, "rev_id"));
		let rev_text_id_idx = try!(maybe_get_column_idx(&results, "rev_text_id"));

		Ok(results.map(|maybe_row| {
			let row = maybe_row.unwrap();
			Revision {
				id: from_value(row[rev_id_idx].to_owned()),
				text_id: from_value(row[rev_text_id_idx].to_owned()),
			}
		})
		.collect::<Vec<Revision>>())
	}
}

#[cfg(test)]
mod test {
	use super::*;
	use util::maybe_connect;

	#[test]
	fn test_rev_get_by_id() {
		let conn = maybe_connect();
		let mut revision_dao = RevisionDao::new(conn);
		let revisions = revision_dao.get_by_id(1);
		assert!(revisions.is_ok());
		revisions.unwrap();
	}

	#[test]
	fn test_get_rev_block() {
		let conn = maybe_connect();
		let mut revision_dao = RevisionDao::new(conn);
		let revisions = revision_dao.get_block(1, 2);
		assert!(revisions.is_ok());
		revisions.unwrap();
	}
}