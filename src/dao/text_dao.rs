use mysql::value::from_value;

use connection::Connection;
use error::Error;
use models::Text;
use util::maybe_get_column_idx;

pub struct TextDao {
	pub conn: Connection
}

impl TextDao {
	pub fn new(conn: Connection) -> Self {
		TextDao {
			conn: conn
		}
	}

	pub fn get_by_id(&mut self, id: u64) -> Result<Vec<Text>, Error> {
		let ref pool = self.conn.pool;
		let results = try!(pool.prep_exec(
			r"select t.* from text t where t.old_id = ?", (id,))
			.map_err(|e| Error::from(e)));

		let id_idx = try!(maybe_get_column_idx(&results, "old_id"));
		let text_idx = try!(maybe_get_column_idx(&results, "old_text"));

		Ok(results.map(|maybe_row| {
			let row = maybe_row.unwrap();
			Text {
				id: from_value(row[id_idx].to_owned()),
				text: from_value(row[text_idx].to_owned()),
			}
		})
		.collect::<Vec<Text>>())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use util::maybe_connect;

	#[test]
	fn test_get_text_by_id() {
		let conn = maybe_connect();
		let mut text_dao = TextDao::new(conn);
		let text = text_dao.get_by_id(1);
		assert!(text.is_ok());
		text.unwrap();
	}
}