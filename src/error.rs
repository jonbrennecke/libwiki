use mysql::error::MyError;
use std::io::Error as IoError;

#[derive(Debug)]
pub enum ErrorKind {
	MySQL(MyError),
	Io(IoError)
}

#[derive(Debug)]
pub struct Error {
	kind: ErrorKind
}

impl Error {
	pub fn new(kind: ErrorKind) -> Self {
		Error {
			kind: kind
		}
	}
}

impl From<MyError> for Error {
	fn from(error: MyError) -> Error {
		Error::new(ErrorKind::MySQL(error))
	}
}

impl From<IoError> for Error {
	fn from(error: IoError) -> Error {
		Error::new(ErrorKind::Io(error))
	}
}