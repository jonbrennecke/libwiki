use std::default::Default;
use kafka::client::KafkaClient;
use kafka::error::Error as KafkaError;

pub struct KafkaConnection {
	client: KafkaClient
}

impl KafkaConnection {
	pub fn new<'a>(opts: KafkaOptions<'a>) -> Result<Self, KafkaError> {
		let url = format!("{host}:{port}", host = opts.host, port = opts.port);
		let mut client = KafkaClient::new(vec!(url.to_owned()));
		try!(client.load_metadata_all());
		Ok(KafkaConnection {
			client: client
		})
	}
}

#[derive(Debug)]
pub struct KafkaOptions<'a> {
	host: &'a str,
	port: i64,
	topic: &'a str,
}

impl<'a> Default for KafkaOptions<'a> {	
	fn default() -> Self {
		KafkaOptions {
			host: "127.0.0.1",
			port: 9092,
			topic: "my-topic"
		}
	}
}