#[derive(Debug, Clone)]
pub struct Category {
	pub id: u64,
	pub title: String,
	pub pages: u64
}