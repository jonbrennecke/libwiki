use mysql::value::{Value, from_value_opt};
use mysql::conn::QueryResult;

use util::maybe_get_column_idx;
use error::Error;

#[derive(Debug)]
pub struct CategoryLink {
	pub from: u64,
	pub to: String,
}

impl CategoryLink {
	pub fn new_with_row_indexes<'a>(row: &Vec<Value>, idxs: &'a CategoryLinkColumnIndexes) -> Result<Self, Error> {
		Ok(CategoryLink {
			from: try!(from_value_opt(row[idxs.from].to_owned())),
			to: try!(from_value_opt(row[idxs.to].to_owned())),
		})
	}
}

pub struct CategoryLinkColumnIndexes {
	pub from: usize,
	pub to: usize
}

impl CategoryLinkColumnIndexes {
	pub fn new<'a>(results: &'a QueryResult<'a>) -> Result<Self, Error> {
		let from = try!(maybe_get_column_idx(results, "cl_from"));
		let to = try!(maybe_get_column_idx(results, "cl_to"));
		Ok(CategoryLinkColumnIndexes {
			from: from,
			to: to,
		})
	}
}