use mysql::value::{Value, from_value};
use mysql::conn::QueryResult;

use util::maybe_get_column_idx;
use error::Error;

#[derive(Debug)]
pub struct Page {
	pub id: u64,
	pub latest: u64,
	pub namespace: u64,
	pub title: String,
}

impl Page {
	pub fn new_with_row_indexes<'a>(row: &Vec<Value>, idxs: &'a PageColumnIndexes) -> Self {
		Page {
			id: from_value(row[idxs.id].to_owned()),
			namespace: from_value(row[idxs.namespace].to_owned()),
			title: from_value(row[idxs.title].to_owned()),
			latest: from_value(row[idxs.latest].to_owned())
		}
	}
}

pub struct PageColumnIndexes {
	pub title: usize,
	pub id: usize,
	pub namespace: usize,
	pub latest: usize
}

impl PageColumnIndexes {
	pub fn new<'a>(results: &'a QueryResult<'a>) -> Result<Self, Error> {
		let title = try!(maybe_get_column_idx(results, "page_title"));
		let id = try!(maybe_get_column_idx(results, "page_id"));
		let namespace = try!(maybe_get_column_idx(results, "page_namespace"));
		let latest = try!(maybe_get_column_idx(results, "page_latest"));
		Ok(PageColumnIndexes {
			title: title,
			id: id,
			namespace: namespace,
			latest: latest,
		})
	}
}