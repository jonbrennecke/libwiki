use models::{Page, Text, CategoryLink};
use util::c_string;

#[derive(Debug)]
pub struct PageTextCategory {
	pub page: Page,
	pub text: Option<Text>,
	pub category_link: Option<CategoryLink>
}

/*=============================
=            C API            =
=============================*/

use std::ffi::CString;
use std::ptr;

#[no_mangle]
pub unsafe extern "C" fn ptc_get_title(ptc: * mut PageTextCategory) -> c_string 
{
	match CString::new(&*(*ptc).page.title) {
		Ok(cstr) => cstr.into_raw(),
		Err(_) => ptr::null() as c_string
	}
}

#[no_mangle]
pub unsafe extern "C" fn ptc_get_category(ptc: * mut PageTextCategory) -> c_string
{
	match (*ptc).category_link {
		Some(ref cl) => match CString::new(&*cl.to) {
			Ok(cstr) => cstr.into_raw(),
			Err(_) => ptr::null() as c_string
		},
		None => ptr::null() as c_string
	}
}

#[no_mangle]
pub unsafe extern "C" fn ptc_get_text(ptc: * mut PageTextCategory) -> c_string 
{
	match (*ptc).text {
		Some(ref text) => match CString::new(&*text.text) {
			Ok(cstr) => cstr.into_raw(),
			Err(_) => ptr::null() as c_string
		},
		None => ptr::null() as c_string
	}
}

/*=====  End of C API  ======*/

#[cfg(test)]
mod tests {
	use connection::{wiki_config, wiki_connect};
	use std::ptr;
	use std::ffi::CString;
	use collections::*;

	#[test]
	fn test_get_text() {
		unsafe {
			let host = "127.0.0.1";
			let user = "root";
			let pwd = "";
			let port = 3306;
			let dbname = "wiki";
			let config = wiki_config(
				CString::new(host).unwrap().as_ptr(), 
				CString::new(user).unwrap().as_ptr(), 
				CString::new(pwd).unwrap().as_ptr(), 
				port, 
				CString::new(dbname).unwrap().as_ptr());
			
			let conn = wiki_connect(config);
			assert!(conn != ptr::null_mut());
			
			let collection = new_page_collection(conn);
			assert!(collection != ptr::null_mut());
		}
	}
}