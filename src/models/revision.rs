use mysql::value::{Value, from_value};
use mysql::conn::QueryResult;

use util::maybe_get_column_idx;
use error::Error;

#[derive(Debug)]
pub struct Revision {
	pub id: u64,
	pub text_id: u64,
}

impl Revision {
	pub fn new_with_row_indexes<'a>(row: Vec<Value>, idxs: &'a RevisionColumnIndexes) -> Self {
		Revision {
			id: from_value(row[idxs.id].to_owned()),
			text_id: from_value(row[idxs.text_id].to_owned()),
		}
	}
}

pub struct RevisionColumnIndexes {
	pub id: usize,
	pub text_id: usize
}

impl RevisionColumnIndexes {
	pub fn new<'a>(results: &'a QueryResult<'a>) -> Result<Self, Error> {
		let text_id = try!(maybe_get_column_idx(results, "rev_text_id"));
		let id = try!(maybe_get_column_idx(results, "rev_id"));
		Ok(RevisionColumnIndexes {
			text_id: text_id,
			id: id,
		})
	}
}