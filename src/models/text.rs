use mysql::value::{Value, from_value_opt};
use mysql::conn::QueryResult;

use util::maybe_get_column_idx;
use error::Error;

#[derive(Debug)]
pub struct Text {
	pub id: u64,
	pub text: String
}

impl Text {
	pub fn new_with_row_indexes<'a>(row: &Vec<Value>, idxs: &'a TextColumnIndexes) 
		-> Result<Self, Error> 
	{
		Ok(Text {
			id: try!(from_value_opt(row[idxs.id].to_owned())),
			text: try!(from_value_opt(row[idxs.text].to_owned())),
		})
	}
}

pub struct TextColumnIndexes {
	pub id: usize,
	pub text: usize
}

impl TextColumnIndexes {
	pub fn new<'a>(results: &'a QueryResult<'a>) -> Result<Self, Error> {
		let text = try!(maybe_get_column_idx(results, "old_text"));
		let id = try!(maybe_get_column_idx(results, "old_id"));
		Ok(TextColumnIndexes {
			text: text,
			id: id,
		})
	}
}