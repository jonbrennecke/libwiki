use dao::PageTextCategoryDao;
use connection::Connection;
use models::PageTextCategory;

pub struct BlockIterator {
	index: usize,	
	block_size: usize,
	dao: PageTextCategoryDao,
}

impl BlockIterator {
	pub fn new(conn: Connection, block_size: usize) -> Self {
		let dao = PageTextCategoryDao::new(conn);
		BlockIterator {
			index: 0,
			block_size: block_size,
			dao: dao
		}
	}
}

impl Iterator for BlockIterator	 {
    type Item = Vec<PageTextCategory>;

    fn next(&mut self) -> Option<Self::Item> {
    	match self.dao.get_several(self.index, self.block_size) {
    		Ok(block) => {
    			self.index += self.block_size;
    			Some(block)
    		},
    		Err(_) => None
    	}
    }
}

#[cfg(test)]
mod test {
	use super::*;
	use util::maybe_connect;

	#[test]
	fn test_large_blocks() {
		let conn = maybe_connect();
		let mut iter = BlockIterator::new(conn, 100);

		for _ in 1..10 {
			let maybe_block = iter.next();
			maybe_block.unwrap();
		}
	}

	#[test]
	fn test_block_iter() {
		let conn = maybe_connect();
		let mut iter = BlockIterator::new(conn, 100);
		let maybe_block = iter.next();
		assert!(maybe_block.is_some());
	}
}