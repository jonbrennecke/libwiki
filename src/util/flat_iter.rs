use std::iter::IntoIterator;
use std::boxed::Box;

pub trait IntoFlatMap<T> where T: Iterator, T::Item: IntoIterator {
	fn into_flat_map(self) 
		-> Box<Iterator<Item=<<T::Item as IntoIterator>::IntoIter as Iterator>::Item> + Sized>;
}

impl<T: 'static> IntoFlatMap<T> for T where T: Iterator, T::Item: IntoIterator {
	fn into_flat_map(self) 
		-> Box<Iterator<Item=<<T::Item as IntoIterator>::IntoIter as Iterator>::Item> + Sized> 
	{
		Box::new(self.flat_map(|item| item.into_iter()))
	}
}

pub struct FlatIterator<F: Iterator + ?Sized>(pub Box<F>);

impl<F: Iterator + ?Sized> Iterator for FlatIterator<F> {
	type Item = <F as Iterator>::Item;

	fn next(&mut self) -> Option<Self::Item> {
		self.0.next()
	}
}

#[cfg(feature = "c_api")]
#[cfg(test)]
mod tests {
	use super::*;
	use util::{BlockIterator, maybe_connect};

	#[test]
	fn test_flat_iter() {
		let conn = maybe_connect();
		let block_iter = BlockIterator::new(conn, 100);
		let mut iter = FlatIterator::<_>(block_iter.into_flat_map());
		let maybe_ptc = iter.next();
		assert!(maybe_ptc.is_some());
		let ptc = maybe_ptc.unwrap();
		assert!(ptc.page.title != "");
	}
}
