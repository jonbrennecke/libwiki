use mysql::conn::QueryResult;
use std::io::{ErrorKind, Error as IoError};

use error::Error;

pub fn maybe_get_column_idx<'a, 'b>(res: &QueryResult<'a>, column: &'b str) -> Result<usize, Error> {
	res.column_index(column).ok_or_else(|| {
		Error::from(IoError::new(ErrorKind::Other, format!("Could not find column `{}`.", column)))
	})
}