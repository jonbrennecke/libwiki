use connection::Connection;
use std::default::Default;

pub fn maybe_connect() -> Connection {
	if let Ok(conn) = Connection::new(Default::default()) {
		return conn;
	}
	panic!("Failed to connect with default options.");
}